'use strict';
const oracledb = require('oracledb');

module.exports = function(Montoapagar) {
  Montoapagar.anular = function (nrotrx,
  									caja,
  									cajero,
  									cb) {
    const ds = Montoapagar.app.dataSources.oracleDb;
    const sql = `CALL PA_PAGOS_BANCO.anularPago(:NROTRX,
    											   :CAJA,
    											   :CAJERO,
                             :ACK_ANULACION)`;
    const params = {
		NROTRX: { val: nrotrx, dir: oracledb.BIND_IN },
		CAJA: { val: caja, dir: oracledb.BIND_IN },
		CAJERO: { val: cajero, dir: oracledb.BIND_IN },
		ACK_ANULACION: { dir: oracledb.BIND_OUT }
	};

    ds.connector.executeSQL(sql, params, {}, function (err, data) {
      if (err) {
        console.log("Error:", err);
	    return cb(err);
      }
      // if(data.outBinds.MENSAJEERROR){
      // 	if(data.outBinds.MENSAJEERROR === "Obligacion inexistente"){
	     //  	return cb({statusCode: 404, msj:data.outBinds.MENSAJEERROR});
      // 	}
      // 	return cb({msj:data.outBinds.MENSAJEERROR});
      // }
      cb(null, data.outBinds);
    });
  }

  Montoapagar.remoteMethod(
    'anular',
    {
      accepts: [
        {arg: 'nrotrx', type: 'string'},
        {arg: 'caja', type: 'string'},
        {arg: 'cajero', type: 'number'},
        ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {path: '/', verb: 'post'}
    }
  );
};
