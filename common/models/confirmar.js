'use strict';
const oracledb = require('oracledb');

module.exports = function(Montoapagar) {
  Montoapagar.confirmar = function (impuesto,
  									conceptoObligacion,
  									numeroObligacionImpuesto,
  									numeroRectificativa,
  									numeroCuota,
  									anioObligacion,
  									formatoIdentificacion,
  									identificacion,
                    sucursalbancaria,
  									caja,
                    cajero,
                    importePago,
                    nrotrx,
                    token,                    
  									cb) {
    const ds = Montoapagar.app.dataSources.oracleDb;
    const sql = `CALL PA_PAGOS_BANCO.ConfirmaPago(:IMPUESTO,
    											   :CONCEPTO_OBLIGACION,
    											   :NUMERO_OBLIGACION_IMPUESTO,
    											   :NUMERO_RECTIFICATIVA,
    											   :NUMERO_CUOTA,
    											   :ANIO_OBLIGACION,
    											   :FORMATO_IDENTIFICACION,
    											   :IDENTIFICACION,
    											   sysdate,
    											   :SUCURSAL_BANCARIA,
    											   :CAJA,
    											   :CAJERO,
    											   :IMPORTE_PAGO,
    											   :NROTRX,
    											   :TOKEN,
                             :ESTADO,
                             :ACK,
                             :MENSAJE_ESTADO)`;
                             
    const params = {
		IMPUESTO: { val: impuesto, dir: oracledb.BIND_IN },
		CONCEPTO_OBLIGACION: { val: conceptoObligacion, dir: oracledb.BIND_IN },
		NUMERO_OBLIGACION_IMPUESTO: { val: numeroObligacionImpuesto, dir: oracledb.BIND_IN },
		NUMERO_RECTIFICATIVA: { val: numeroRectificativa, dir: oracledb.BIND_IN },
		NUMERO_CUOTA: { val: numeroCuota, dir: oracledb.BIND_IN },
		ANIO_OBLIGACION: { val: anioObligacion, dir: oracledb.BIND_IN },
		FORMATO_IDENTIFICACION: { val: formatoIdentificacion, dir: oracledb.BIND_IN },
		IDENTIFICACION: { val: identificacion, dir: oracledb.BIND_IN },
    SUCURSAL_BANCARIA: { val: sucursalbancaria, dir: oracledb.BIND_IN },
    CAJA: { val: caja, dir: oracledb.BIND_IN },
    CAJERO: { val: cajero, dir: oracledb.BIND_IN },
		IMPORTE_PAGO: { val: importePago, dir: oracledb.BIND_IN },
	  NROTRX: { val: nrotrx, dir: oracledb.BIND_IN },
    TOKEN: { val: token, dir: oracledb.BIND_IN },
  	ESTADO: { dir: oracledb.BIND_OUT },
		ACK: { dir: oracledb.BIND_OUT },
		MENSAJE_ESTADO: { dir: oracledb.BIND_OUT }
			};

    ds.connector.executeSQL(sql, params, {}, function (err, data) {
      if (err) {
        console.log("Error:", err);
	    return cb(err);
      }
      // if(data.outBinds.MENSAJEERROR){
      // 	if(data.outBinds.MENSAJEERROR === "Obligacion inexistente"){
	     //  	return cb({statusCode: 404, msj:data.outBinds.MENSAJEERROR});
      // 	}
      // 	return cb({msj:data.outBinds.MENSAJEERROR});
      // }
      cb(null, data.outBinds);
    });
  }

  Montoapagar.remoteMethod(
    'confirmar',
    {
      accepts: [
        {arg: 'impuesto', type: 'string'},
        {arg: 'conceptoObligacion', type: 'string'},
        {arg: 'numeroObligacionImpuesto', type: 'number'},
        {arg: 'numeroRectificativa', type: 'number'},
        {arg: 'numeroCuota', type: 'number'},
        {arg: 'anioObligacion', type: 'number'},
        {arg: 'formatoIdentificacion', type: 'string'},
        {arg: 'identificacion', type: 'string'},
        {arg: 'sucursalbancaria', type: 'string'},
        {arg: 'caja', type: 'string'},
        {arg: 'cajero', type: 'string'},
        {arg: 'importePago', type: 'number'},
        {arg: 'nrotrx', type: 'string'},
        {arg: 'token', type: 'string'},
                
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {path: '/', verb: 'post'}
    }
  );
};
