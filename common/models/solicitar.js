'use strict';
const oracledb = require('oracledb');

module.exports = function(Montoapagar) {
  Montoapagar.solicitar = function (impuesto,
  									conceptoObligacion,
  									numeroObligacionImpuesto,
  									numeroRectificativa,
  									numeroCuota,
  									anioObligacion,
  									formatoIdentificacion,
  									identificacion,
  									importePago,
  									cb) {
    const ds = Montoapagar.app.dataSources.oracleDb;
    const sql = `CALL PA_PAGOS_BANCO.SolicitarPago(:IMPUESTO,
    											   :CONCEPTO_OBLIGACION,
    											   :NUMERO_OBLIGACION_IMPUESTO,
    											   :NUMERO_RECTIFICATIVA,
    											   :NUMERO_CUOTA,
    											   :ANIO_OBLIGACION,
    											   :FORMATO_IDENTIFICACION,
    											   :IDENTIFICACION,
    											   sysdate,
    											   :IMPORTE_PAGO,
    											   :MENSAJEERROR,
    											   :COBROHABILITADO,
    											   :ESTADODELAOBLIGACION,
    											   :TIPOINTERES,
    											   :FECHAVENCIMIENTOORIGINAL,
    											   :IMPORTEORIGINAL,
    											   :IMPORTEEXENCION,
    											   :IMPORTEINTERESES,
    											   :IMPORTEMULTAFORMAL,
    											   :IMPORTEMULTAOMISION,
    											   :IMPORTEMULTATASA,
    											   :IMPORTETOTAL,
    											   :TOKEN)`;
    const params = {
		IMPUESTO: { val: impuesto, dir: oracledb.BIND_IN },
		CONCEPTO_OBLIGACION: { val: conceptoObligacion, dir: oracledb.BIND_IN },
		NUMERO_OBLIGACION_IMPUESTO: { val: numeroObligacionImpuesto, dir: oracledb.BIND_IN },
		NUMERO_RECTIFICATIVA: { val: numeroRectificativa, dir: oracledb.BIND_IN },
		NUMERO_CUOTA: { val: numeroCuota, dir: oracledb.BIND_IN },
		ANIO_OBLIGACION: { val: anioObligacion, dir: oracledb.BIND_IN },
		FORMATO_IDENTIFICACION: { val: formatoIdentificacion, dir: oracledb.BIND_IN },
		IDENTIFICACION: { val: identificacion, dir: oracledb.BIND_IN },
		IMPORTE_PAGO: { val: importePago, dir: oracledb.BIND_IN },
		MENSAJEERROR:  { dir: oracledb.BIND_OUT },
		ESTADODELAOBLIGACION: { dir: oracledb.BIND_OUT },
		TIPOINTERES: { dir: oracledb.BIND_OUT },
		COBROHABILITADO: { dir: oracledb.BIND_OUT },
		FECHAVENCIMIENTOORIGINAL: { dir: oracledb.BIND_OUT },
		IMPORTEORIGINAL: { dir: oracledb.BIND_OUT },
		IMPORTEINTERESES: {  dir: oracledb.BIND_OUT },
		IMPORTEMULTAFORMAL: {  dir: oracledb.BIND_OUT },
		IMPORTEMULTAOMISION: { dir: oracledb.BIND_OUT },
		IMPORTEMULTATASA: { dir: oracledb.BIND_OUT },
		IMPORTEEXENCION: { dir: oracledb.BIND_OUT },
		IMPORTETOTAL: { dir: oracledb.BIND_OUT },
		TOKEN: { dir: oracledb.BIND_OUT }
	};

    ds.connector.executeSQL(sql, params, {}, function (err, data) {
      if (err) {
        console.log("Error:", err);
	    return cb(err);
      }
       if(data.outBinds.MENSAJEERROR){
       	if(data.outBinds.MENSAJEERROR === "Obligacion inexistente"){
	      	return cb({statusCode: 404, msj:data.outBinds.MENSAJEERROR});
       	}
       	return cb({msj:data.outBinds.MENSAJEERROR});
       }
      cb(null, data.outBinds);
    });
  }

  Montoapagar.remoteMethod(
    'solicitar',
    {
      accepts: [
        {arg: 'impuesto', type: 'string'},
        {arg: 'conceptoObligacion', type: 'string'},
        {arg: 'numeroObligacionImpuesto', type: 'number'},
        {arg: 'numeroRectificativa', type: 'number'},
        {arg: 'numeroCuota', type: 'number'},
        {arg: 'anioObligacion', type: 'number'},
        {arg: 'formatoIdentificacion', type: 'string'},
        {arg: 'identificacion', type: 'string'},
        {arg: 'importePago', type: 'number'},
      ],
      returns: {arg: 'result', type: 'object', root: true},
      http: {path: '/', verb: 'post'}
    }
  );
};
