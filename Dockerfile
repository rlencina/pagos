FROM collinestes/docker-node-oracle

ADD package.json .

RUN npm install

ADD server ./server

ADD common ./common

EXPOSE 5001

CMD node ./server/server.js
