# API pagos

Este proyecto consolida los servicios referentes a pagos

## Requerimientos:

* NodeJS 4.x o superior, recomendado 8 o superior

## Instalacion de loopback-cli

Loopback posee una interfaz de linea de comando que facilita la generación de aplicaciones, modelos, bases de datos, etc.
Para instalarlo se debe ejecuatar el comando:

````
$ npm install -g loopback-cli

````

## Startup del repositorio
Para ejecutar el codigo del repositorio se deberá

1. Instalar dependencias

	```
	 $ npm install
	```

2. Instalar conector de base de datos:
	* Oracle

	 ```
	 $ npm install loopback-connector-oracle --save
	 ```
	 
	* Postgres

	 ```
	 $ npm install loopback-connector-postgres --save
	 ```
	* MySQL 

	 ```
	 $ npm install loopback-connector-mysql --save
	 ```
	 
	 Referencias: https://loopback.io/doc/en/lb2/Connectors-reference.html

3. Configurar base de datos dentro del archivo server/datasources.js indicando:

	```
	  "mysqlAuth": { // Nombre con el que se referencia en la aplicación a la base de datos
	    "host": "10.42.14.93", // Hostname o ip del servidor de base de datos
	    "port": 3306, // Puerto del servidor de base de datos
	    "database": "auth", // Nombre de la instancia de base de datos
	    "password": ").ae54FZb++x8K>F", // Password 
	    "name": "mysqlAuth", // Nombre de referencia (respetar con el indicado anteriormente) 
	    "connector": "mysql", // Nombre del conector a utilizar
	    "user": "auth" // Usuario de la base de datos
  	}
	```
4. Ejecutar el servidor:
	
	```
	$ node .
	```

Links de interes:

* https://loopback.io/getting-started/
* https://loopback.io/doc/en/lb3/Getting-started-with-LoopBack.html